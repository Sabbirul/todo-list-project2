<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TodoController;

//Routes for user Login & SignUp
Route::post("login",[UserController::class,'index'])->name('login');
Route::post("signUp",[UserController::class,'signUp'])->name('signUp');
Route::post("logout",[UserController::class,'logout'])->name('logout');

Route::group(['middleware' => 'auth:sanctum'], function(){
    //All secure URL's

    //Todo List all process by single route using apiResource
    Route::apiResource('todo',TodoController::class);
});


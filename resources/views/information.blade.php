@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Information:') }}</div>
                <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <h1>Your server is running Successfully</h1>
                            <h2>Database created and migration file migrated Successfully</h2>
                            <h3>Your API is Good to Go...</h3>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
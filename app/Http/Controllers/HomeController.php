<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {       
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //To create key generate,database
    public function index()
    {
        \Artisan::call('key:generate');
        $databaseName = config('database.connections.mysql.database');
        \Artisan::call('db:create '.$databaseName);
         return redirect()->route('config');
    }

    //For Finally migration command and instruction page
    public function confirmation()
    { 
        Artisan::call('db:wipe');
        Artisan::call('migrate'); 
        return view('information');
    }
}

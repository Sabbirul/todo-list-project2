<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTodoRequest;
use App\Http\Requests\UpdateTodoRequest;
use Illuminate\Http\Request;
use Auth;

use App\Models\Todo;


class TodoController extends Controller
{

    //Default method to get specific user all To-Do Lists data
    public function index()
    {
        $user_id = Auth::id();
        $todo_lists = Todo::where('user_id',$user_id)->get();

        return response()->json([
            'todo_lists' => $todo_lists
        ]);
    }

    //Method to save an user To-Do Lists information
    public function store(Request $request)
    {
        //To save to do list information
        $todo = Todo::create($request->all());

        return response()->json([
            'message' => 'To-Do List created successfully',
            'todo_lists' => $todo
        ]);
    }

    //Method to find specific To-Do Lists
    public function show($id)
    {
        $todo = Todo::findOrFail($id);
        
        return response()->json([
            'todo_list' => $todo
        ]);
    }

    //Method for updating specific To-Do Lists
    public function update(Request $request,$id)
    {
        $todo = Todo::findOrFail($id);
        $todo->user_id = $request->user_id;
        $todo->name = $request->name;
        $todo->task_list = $request->task_list;
        $todo->completed = $request->completed;
        $todo->save();

        return response()->json([
            'message' => 'To-Do List updated successfully',
            'todo_list' => $todo
        ]);
    }

    //Method for deleting specific To-Do Lists
    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();

        return response()->json([
            'message' => 'To-Do List deleted successfully'
        ]);
    }
}
